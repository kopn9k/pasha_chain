package com.pashachain.core.blockchain.block.logic;

import com.pashachain.core.blockchain.block.crypto.Hasher;
import com.pashachain.core.blockchain.block.model.Block;
import org.springframework.stereotype.Component;

@Component
public class BlockUtil {

    private Hasher hasher;

    public BlockUtil(Hasher hasher) {
        this.hasher =  hasher;
    }

    public Block genesis() {
        return new Block(Block.GENESIS_TIMESTAMP, Block.GENESIS_PREVIOUS_HASH,
                Block.GENESIS_HASH, Block.GENESIS_DATA);
    }

    public Block mineBlock(long currentTimestamp, Block lastBlock, String data) {
        String previousHash = lastBlock.getHash();
        String hash = hasher.hash(currentTimestamp,previousHash, data);
        return new Block(currentTimestamp, previousHash, hash, data);
    }

}

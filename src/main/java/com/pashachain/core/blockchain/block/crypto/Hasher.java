package com.pashachain.core.blockchain.block.crypto;

import org.bouncycastle.jcajce.provider.digest.Keccak;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Component
public class Hasher {

    private final Keccak.Digest256 keccak;

    public Hasher(Keccak.Digest256 hasher) {
        this.keccak = hasher;
    }


    public String hash(long timestamp, String previousHash, String data) {
        String originalString = timestamp + previousHash + data;
        byte[] hashBytes = keccak.digest(originalString.getBytes(StandardCharsets.UTF_8));
        return new String(Hex.encode(hashBytes));
    }
}

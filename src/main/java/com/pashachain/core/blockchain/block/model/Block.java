package com.pashachain.core.blockchain.block.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@AllArgsConstructor
@Getter
@Data
public class Block {

    public final static long GENESIS_TIMESTAMP = 99999L;
    public final static String GENESIS_PREVIOUS_HASH = "_______";
    public final static String GENESIS_HASH = "f1r57-h45h";
    public final static String GENESIS_DATA = "";

    private final long timestamp;
    private final String previousHash;
    private final String hash;
    private final String data;
}

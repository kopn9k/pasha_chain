package com.pashachain.core.blockchain.blockchain.logic;

import com.pashachain.core.common.util.CurrentTime;
import com.pashachain.core.blockchain.block.logic.BlockUtil;
import com.pashachain.core.blockchain.block.model.Block;
import com.pashachain.core.blockchain.blockchain.model.BlockChain;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;


@Component
@Slf4j
public class BlockChainUtil {

    private final BlockChainValidator blockChainValidator;
    private final BlockUtil blockUtil;

    public BlockChainUtil(BlockUtil blockUtil, BlockChainValidator blockChainValidator) {
        this.blockUtil = blockUtil;
        this.blockChainValidator = blockChainValidator;
    }

    @Transactional
    public Block addBlock(String data, BlockChain blockChain) {
        List<Block> chain = blockChain.getChain();
        Block minedBlocke = blockUtil.mineBlock(CurrentTime.getCurrentTime(), getLastBlock(blockChain), data);
        chain.add(minedBlocke);
        return minedBlocke;
    }

    private Block getLastBlock(BlockChain blockChain) {
        List<Block> chain = blockChain.getChain();
        int length = chain.size();
        return chain.get(length - 1);
    }

    @Transactional
    public boolean replaceChain(BlockChain blockChain, List<Block> chain) {
        List<Block> currentChain =  blockChain.getChain();
        if (chain.size() <= currentChain.size()) {
            log.info("In BlockChainUtil replaceChain {}{}, chain is smaller then previous chain", blockChain, chain);
            return false;
        } else if (!blockChainValidator.isChainValid(chain)) {
            log.info("In BlockChainUtil replaceChain {}{}, chain is invalid", blockChain, chain);
            return false;
        }

        blockChain.getChain().clear();
        blockChain.getChain().addAll(chain);
        log.info("In BlockChainUtil replaceChain {}{}, chain is successfully replaced", blockChain, chain);
        return true;
    }
}

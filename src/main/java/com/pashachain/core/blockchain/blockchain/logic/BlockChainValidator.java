package com.pashachain.core.blockchain.blockchain.logic;

import com.pashachain.core.blockchain.block.crypto.Hasher;
import com.pashachain.core.blockchain.block.logic.BlockUtil;
import com.pashachain.core.blockchain.block.model.Block;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BlockChainValidator {

    private final BlockUtil blockUtil;
    private final Hasher hasher;

    public BlockChainValidator(BlockUtil blockUtil, Hasher hasher) {
        this.blockUtil = blockUtil;
        this.hasher = hasher;
    }



    public boolean isChainValid(List<Block> chain) {
        Block firstBlock = chain.get(0);
        Block genesis = blockUtil.genesis();
        if (!genesis.equals(firstBlock)) {
            return false;
        }

        for(int i = 1; i < chain.size(); i++) {
            Block block =  chain.get(i);
            Block previousBlock =  chain.get(i - 1);

            if (!block.getPreviousHash().equals(previousBlock.getHash()) ||
                    !block.getHash().equals(hasher.hash(block.getTimestamp(), block.getPreviousHash(), block.getData()))) {
                return false;
            }
        }

        return true;
    }
}

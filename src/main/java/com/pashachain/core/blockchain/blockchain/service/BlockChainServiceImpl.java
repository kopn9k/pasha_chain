package com.pashachain.core.blockchain.blockchain.service;

import com.pashachain.core.blockchain.block.model.Block;
import com.pashachain.core.blockchain.blockchain.logic.BlockChainUtil;
import com.pashachain.core.blockchain.blockchain.model.BlockChain;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class BlockChainServiceImpl implements BlockChainService {

    private final BlockChain blockChain;
    private final BlockChainUtil blockChainUtil;

    public BlockChainServiceImpl(BlockChain blockChain, BlockChainUtil blockChainUtil) {
        this.blockChain = blockChain;
        this.blockChainUtil = blockChainUtil;
    }


    @Override
    public List<Block> getChain() {
        return blockChain.getChain();
    }

    @Override
    public Block addBlock(String data) {
        return blockChainUtil.addBlock(data, blockChain);
    }
}

package com.pashachain.core.blockchain.blockchain.service;

import com.pashachain.core.blockchain.block.model.Block;
import java.util.List;

public interface BlockChainService {

    List<Block> getChain();

    Block addBlock(String data);
}

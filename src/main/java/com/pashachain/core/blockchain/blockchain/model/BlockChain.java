package com.pashachain.core.blockchain.blockchain.model;

import com.pashachain.core.blockchain.block.logic.BlockUtil;
import com.pashachain.core.blockchain.block.model.Block;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
/* todo: 1.Singleton
         2.TradeSafe
 */
@Getter
@EqualsAndHashCode
@ToString
public class BlockChain {

    private List<Block> chain;


    public BlockChain(BlockUtil blockUtil){
        chain = new ArrayList<>();
        chain.add(blockUtil.genesis());
    }

}

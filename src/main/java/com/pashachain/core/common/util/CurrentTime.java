package com.pashachain.core.common.util;

import java.util.Calendar;

public class CurrentTime {

    public static long getCurrentTime() {
        return Calendar.getInstance().getTimeInMillis();
    }
}

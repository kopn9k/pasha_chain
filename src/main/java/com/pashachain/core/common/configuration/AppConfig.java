package com.pashachain.core.common.configuration;

import org.bouncycastle.jcajce.provider.digest.Keccak;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class AppConfig {

    @Bean(name = "org.bouncycastle.jcajce.provider.digest.Keccak$Digest256", value = "org.bouncycastle.jcajce.provider.digest.Keccak$Digest256")
    public Keccak.Digest256 keccakDigest256() {
        return new Keccak.Digest256();

    }
}

package com.pashachain.webapp.blockchain;

import com.pashachain.core.blockchain.block.model.Block;
import com.pashachain.core.blockchain.blockchain.service.BlockChainService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/blockchain/chain/")
public class BlockChainRestControllerV1 {

    private final BlockChainService blockChainService;

    public BlockChainRestControllerV1(BlockChainService blockChainService) {
        this.blockChainService = blockChainService;
    }


    @GetMapping(value = "")
    public List<Block> getBlocks() {
        return this.blockChainService.getChain();
    }

    @PostMapping(value = "")
    public ResponseEntity<Block> mineBlock(@RequestBody String data) {
        Block minedBlock = blockChainService.addBlock(data);
        return new ResponseEntity<>(minedBlock, HttpStatus.OK);
    }
}

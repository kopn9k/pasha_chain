package com.pashachain.core.blockchain.block.logic;

import com.pashachain.core.common.util.CurrentTime;
import com.pashachain.core.blockchain.block.crypto.Hasher;
import com.pashachain.core.blockchain.block.model.Block;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BlockUtilTest {

    @Mock
    Hasher hasher;

    @InjectMocks
    BlockUtil blockUtil;

    public final static long CURRENT_TIME = CurrentTime.getCurrentTime();
    public final static Block CORRECT_GENESIS_BLOCK = new Block(Block.GENESIS_TIMESTAMP,
            Block.GENESIS_PREVIOUS_HASH, Block.GENESIS_HASH, Block.GENESIS_DATA);
    public final static String MINED_HASH = Block.GENESIS_HASH + "todo-hash";
    public final static String DATA = "foo";
    public final static Block CORRECT_MINED_BLOCK = new Block(CURRENT_TIME,
            Block.GENESIS_HASH, MINED_HASH, DATA);

    @Test
    public void shouldReturnGenesisBlock() {
        //given
        //when
        Block genesisBlock = blockUtil.genesis();
        //then
        Assert.assertEquals(CORRECT_GENESIS_BLOCK, genesisBlock);
    }

    @Test
    public void shouldGenerateBlockBasedOnGenesisBlockAndInputData() {
        //given
        Mockito.when(hasher.hash(CURRENT_TIME, Block.GENESIS_HASH, DATA))
                .thenReturn(MINED_HASH);
        //when
        Block minedBlock = blockUtil.mineBlock(CURRENT_TIME, CORRECT_GENESIS_BLOCK, DATA);
        //then
        Assert.assertEquals(CORRECT_MINED_BLOCK, minedBlock);
    }
}

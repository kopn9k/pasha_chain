package com.pashachain.core.blockchain.block.model;

import org.junit.Assert;
import org.junit.Test;

public class BlockTest {

    public final static String CORRECT_TO_STRING_VALUE = "Block(timestamp=" + Block.GENESIS_TIMESTAMP
            + ", previousHash=" + Block.GENESIS_PREVIOUS_HASH + ", hash=" + Block.GENESIS_HASH
            + ", data=" + Block.GENESIS_DATA + ")";

    @Test
    public void shouldPrintBlockInReadableFormat() {
        //given
        Block block = new Block(Block.GENESIS_TIMESTAMP, Block.GENESIS_PREVIOUS_HASH,
                Block.GENESIS_HASH, Block.GENESIS_DATA);
        //when
        String blockToString = block.toString();
        //then
        Assert.assertEquals(CORRECT_TO_STRING_VALUE, blockToString);
    }
}

package com.pashachain.core.blockchain.block.crypto;

import org.bouncycastle.jcajce.provider.digest.Keccak;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HasherTest {

    @Autowired
    Keccak.Digest256 keccak;

    public final static long TIME = 99999L;
    public final static String PREVIOUS_HASH = "f1r57-h45h";
    public final static String DATA = "bar";
    public final static String EXPECTED_HASH = "beefa18590e13caf1f70bc7f2d79899137975ecb106370dc92c691e350db45d2";

    @Test
    public void shouldReturnCorrectHashValue() {
        //given
        Hasher hasher =  new Hasher(keccak);
        //when
        String result =  hasher.hash(TIME, PREVIOUS_HASH, DATA);
        //then
        Assert.assertEquals(EXPECTED_HASH, result);
    }


}

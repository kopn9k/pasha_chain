package com.pashachain.core.blockchain.blockchain.service;

import com.pashachain.core.blockchain.block.model.Block;
import com.pashachain.core.blockchain.blockchain.logic.BlockChainUtil;
import com.pashachain.core.blockchain.blockchain.model.BlockChain;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class BlockChainServiceTest {

    public final static Block CORRECT_BLOCK = new Block(Block.GENESIS_TIMESTAMP, Block.GENESIS_PREVIOUS_HASH,
            Block.GENESIS_HASH, Block.GENESIS_DATA);
    public final static List<Block> CORRECT_CHAIN = new ArrayList<>(Collections.singletonList(CORRECT_BLOCK));
    public final static String DATA = "bpp";

    @Mock
    BlockChain blockChain;
    @Mock
    BlockChainUtil blockChainUtil;

    @InjectMocks
    BlockChainServiceImpl blockChainService;

    @Test
    public void shouldReturnCorrectChain() {
        //given
        Mockito.when(blockChain.getChain()).thenReturn(CORRECT_CHAIN);
        //when
        List<Block> result = blockChainService.getChain();
        //then
        Assert.assertEquals(CORRECT_CHAIN, result);
    }


    @Test
    public void shouldReturnAddedBlock() {
        //given
        Mockito.when(blockChainUtil.addBlock(DATA, blockChain))
                .thenReturn(CORRECT_BLOCK);
        //when
        Block result = blockChainService.addBlock(DATA);
        //then
        Assert.assertEquals(CORRECT_BLOCK, result);
    }
}

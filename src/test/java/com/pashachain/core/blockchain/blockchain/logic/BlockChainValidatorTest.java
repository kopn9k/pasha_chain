package com.pashachain.core.blockchain.blockchain.logic;

import com.pashachain.core.common.util.CurrentTime;
import com.pashachain.core.blockchain.block.crypto.Hasher;
import com.pashachain.core.blockchain.block.logic.BlockUtil;
import com.pashachain.core.blockchain.block.model.Block;
import com.pashachain.core.blockchain.blockchain.model.BlockChain;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class BlockChainValidatorTest {

    public final static String DATA = "baz";
    public final static long CURRENT_TIME = CurrentTime.getCurrentTime();
    public final static Block GENESIS_BLOCK = new Block(Block.GENESIS_TIMESTAMP, Block.GENESIS_PREVIOUS_HASH,
            Block.GENESIS_HASH, Block.GENESIS_DATA);
    public final static Block WRONG_GENESIS_BLOCK =  new Block(Block.GENESIS_TIMESTAMP, Block.GENESIS_PREVIOUS_HASH,
            Block.GENESIS_HASH, DATA);
    public final static String HASH = CURRENT_TIME + Block.GENESIS_HASH + DATA;
    public final static Block MINED_BLOCK = new Block(CURRENT_TIME, Block.GENESIS_HASH,
            HASH, DATA);


    @Mock
    private BlockUtil blockUtil;
    @Mock
    private Hasher hasher;

    @InjectMocks
    public BlockChainValidator blockChainValidator;
    @InjectMocks
    public BlockChainUtil blockChainUtil;

    public BlockChain blockChain;

    @Before
    public void createBlockChain() {
        Mockito.when(blockUtil.genesis()).thenReturn(GENESIS_BLOCK);
        blockChain =  new BlockChain(blockUtil);
    }

    @Test
    public void isChainValidShouldReturnTrue() {
        //given
        Mockito.when(blockUtil.mineBlock(Mockito.anyLong(), Mockito.eq(GENESIS_BLOCK), Mockito.eq(DATA)))
                .thenReturn(MINED_BLOCK);
        Mockito.when(hasher.hash(Mockito.anyLong(), Mockito.anyString(), Mockito.anyString()))
                .thenAnswer(i -> i.getArgument(0).toString() + i.getArgument(1).toString() +
                        i.getArgument(2).toString());
        blockChainUtil.addBlock(DATA, blockChain);
        List<Block> chain = blockChain.getChain();
        //when
        boolean result = blockChainValidator.isChainValid(chain);
        //then
        Assert.assertTrue(result);
    }

    @Test
    public void isChainValidShouldReturnFalseBecauseNotStartedWithGenesisBlock() {
        //given
        List<Block> chain = blockChain.getChain();
        chain.set(0, WRONG_GENESIS_BLOCK);
        blockChainUtil.addBlock(DATA, blockChain);
        //when
        boolean result = blockChainValidator.isChainValid(chain);
        //then
        Assert.assertFalse(result);
    }

    @Test
    public void isChainValidShouldReturnFalseBecausePreviousHashNotEqualPreviousBlockHash() {
        //given
        Mockito.when(blockUtil.mineBlock(Mockito.anyLong(), Mockito.eq(GENESIS_BLOCK), Mockito.eq(DATA)))
                .thenReturn(MINED_BLOCK);
        blockChainUtil.addBlock(DATA, blockChain);
        List<Block> chain = blockChain.getChain();
        Block lastBlock = chain.get(chain.size()-1);
        Block wrongLastBlock = new Block(lastBlock.getTimestamp(), "wrongPreviousHash",
                lastBlock.getHash(), lastBlock.getData());
        chain.set(chain.size()-1, wrongLastBlock);
        //when
        boolean result = blockChainValidator.isChainValid(chain);
        //then
        Assert.assertFalse(result);
    }

    @Test
    public void isChainValidShouldReturnFalseBecauseBlockHashCalculatedWrong() {
        //given
        Mockito.when(blockUtil.mineBlock(Mockito.anyLong(), Mockito.eq(GENESIS_BLOCK), Mockito.eq(DATA)))
                .thenReturn(MINED_BLOCK);
        Mockito.when(hasher.hash(Mockito.anyLong(), Mockito.anyString(), Mockito.anyString()))
                .thenAnswer(i -> i.getArgument(0).toString() + i.getArgument(1).toString() +
                        i.getArgument(2).toString());
        blockChainUtil.addBlock(DATA, blockChain);
        List<Block> chain = blockChain.getChain();
        Block lastBlock = chain.get(chain.size()-1);
        Block wrongLastBlock = new Block(lastBlock.getTimestamp(), lastBlock.getPreviousHash(),
                "wrongHash", lastBlock.getData());
        chain.set(chain.size()-1, wrongLastBlock);
        //when
        boolean result = blockChainValidator.isChainValid(chain);
        //then
        Assert.assertFalse(result);

    }
}

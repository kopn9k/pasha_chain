package com.pashachain.core.blockchain.blockchain.model;

import com.pashachain.core.blockchain.block.logic.BlockUtil;
import com.pashachain.core.blockchain.block.model.Block;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BlockChainTest {

    public final static Block GENESIS_BLOCK = new Block(Block.GENESIS_TIMESTAMP, Block.GENESIS_PREVIOUS_HASH,
            Block.GENESIS_HASH, Block.GENESIS_DATA);

    @Mock
    public BlockUtil blockUtil;

    public BlockChain blockChain;

    @Before
    public void createBlockChain() {
        Mockito.when(blockUtil.genesis()).thenReturn(GENESIS_BLOCK);
        blockChain =  new BlockChain(blockUtil);
    }

    @Test
    public void shouldStartWithGenesisBlock() {
        //given
        //when
        Block firstBlock = blockChain.getChain().get(0);
        //then
        Assert.assertEquals(GENESIS_BLOCK, firstBlock);
    }
}

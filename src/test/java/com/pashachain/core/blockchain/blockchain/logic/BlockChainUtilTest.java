package com.pashachain.core.blockchain.blockchain.logic;

import com.pashachain.core.common.util.CurrentTime;
import com.pashachain.core.blockchain.block.logic.BlockUtil;
import com.pashachain.core.blockchain.block.model.Block;
import com.pashachain.core.blockchain.blockchain.model.BlockChain;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(MockitoJUnitRunner.class)
public class BlockChainUtilTest {

    public final static String DATA = "baz";
    public final static long CURRENT_TIME = CurrentTime.getCurrentTime();
    public final static Block GENESIS_BLOCK = new Block(Block.GENESIS_TIMESTAMP, Block.GENESIS_PREVIOUS_HASH,
                    Block.GENESIS_HASH, Block.GENESIS_DATA);
    public final static String HASH = CURRENT_TIME + Block.GENESIS_HASH + DATA;
    public final static Block MINED_BLOCK = new Block(CURRENT_TIME, Block.GENESIS_HASH,
            HASH, DATA);


    @Before
    public void createBlockChain() {
        Mockito.when(blockUtil.genesis()).thenReturn(GENESIS_BLOCK);
        blockChain =  new BlockChain(blockUtil);
    }

    @Mock
    private BlockUtil blockUtil;
    @Mock
    BlockChainValidator blockChainValidator;

    @InjectMocks
    public BlockChainUtil blockChainUtil;

    public BlockChain blockChain;

    @Test
    public void shouldAddNewBlock() {
        //given
        Mockito.when(blockUtil.mineBlock(Mockito.anyLong(), Mockito.eq(GENESIS_BLOCK), Mockito.eq(DATA)))
                .thenReturn(MINED_BLOCK);
        //when
        Block result = blockChainUtil.addBlock(DATA, blockChain);
        //then
        List<Block> chain = blockChain.getChain();
        int length = chain.size();
        Block lastBlock = chain.get(length - 1);
        Assert.assertEquals(MINED_BLOCK, lastBlock);
        Assert.assertEquals(result, MINED_BLOCK);
    }

    @Test
    public void shouldReplaceChain() {
        //given
        Mockito.when(blockChainValidator.isChainValid(Mockito.anyList())).thenReturn(true);
        blockChainUtil.addBlock(DATA, blockChain);
        List<Block> chain =  new ArrayList<>();
        chain.add(MINED_BLOCK);
        chain.add(MINED_BLOCK);
        chain.add(MINED_BLOCK);
        //when
        boolean result = blockChainUtil.replaceChain(blockChain, chain);
        //then
        Assert.assertTrue(result);
        Assert.assertEquals(chain, blockChain.getChain());
    }

    @Test
    public void shouldNotReplaceChainBecauseItIsInvalid() {
        //given
        Mockito.when(blockChainValidator.isChainValid(Mockito.anyList())).thenReturn(false);
        blockChainUtil.addBlock(DATA, blockChain);
        List<Block> chain =  new ArrayList<>();
        chain.add(MINED_BLOCK);
        chain.add(MINED_BLOCK);
        chain.add(MINED_BLOCK);
        //when
        boolean result = blockChainUtil.replaceChain(blockChain, chain);
        //then
        Assert.assertFalse(result);
    }

    @Test
    public void shouldNotReplaceChainBecauseItIsSmaller() {
        //given
        blockChainUtil.addBlock(DATA, blockChain);
        List<Block> chain =  new ArrayList<>();
        chain.add(MINED_BLOCK);
        //when
        boolean result = blockChainUtil.replaceChain(blockChain, chain);
        //then
        Assert.assertFalse(result);
    }

}

package com.pashachain.webapp.blockchain;

import com.pashachain.core.blockchain.block.model.Block;
import com.pashachain.core.blockchain.blockchain.service.BlockChainService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class BlockChainRestControllerTest {

    public final static Block CORRECT_BLOCK = new Block(Block.GENESIS_TIMESTAMP, Block.GENESIS_PREVIOUS_HASH,
                    Block.GENESIS_HASH, Block.GENESIS_DATA);
    public final static List<Block> CORRECT_CHAIN = new ArrayList<>(Collections.singletonList(CORRECT_BLOCK));
    public final static String DATA = "bpp";
    public final static ResponseEntity<Block> CORRECT_RESPONSE_ENTITY_WITH_MINED_BLOCK =
            new ResponseEntity<>(CORRECT_BLOCK, HttpStatus.OK);

    @Mock
    BlockChainService blockChainService;

    @InjectMocks
    BlockChainRestControllerV1 blockChainController;

    @Test
    public void shouldReturnCorrectResponse() {
        //given
        Mockito.when(blockChainService.getChain()).thenReturn(CORRECT_CHAIN);
        //when
        List<Block> result = blockChainController.getBlocks();
        //then
        Assert.assertEquals(CORRECT_CHAIN, result);

    }

    public void shouldReturnCorrectResponseWithMinedBlock() {
        //given
        Mockito.when(blockChainService.addBlock(DATA)).thenReturn(CORRECT_BLOCK);
        //when
        ResponseEntity<Block> result = blockChainController.mineBlock(DATA);
        //then
        Assert.assertEquals(CORRECT_RESPONSE_ENTITY_WITH_MINED_BLOCK, result);
    }
}
